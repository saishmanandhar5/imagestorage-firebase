import React, { useEffect, useState } from "react";

import {
  getAuth,
  signInWithEmailAndPassword,
  GoogleAuthProvider,
  signInWithPopup,
} from "firebase/auth";
import { Link, useNavigate } from "react-router-dom";

export default function Login() {
  const provider = new GoogleAuthProvider();
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const auth = getAuth();

  const loginUser = async () => {
    await signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        navigate("/image");
        localStorage.setItem("user", user);
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert(errorCode);
      });
  };

  const googleLogin = async () => {
    console.log("clicked");

    await signInWithPopup(auth, provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);

        const token = credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        console.log("user is", user);
        localStorage.setItem("user", user.displayName);
        navigate("/image");
      })
      .catch((error) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.customData.email;

        // The AuthCredential type that was used.
        const credential = GoogleAuthProvider.credentialFromError(error);
      });
  };

  return (
    <>
      <div className="container" style={{ padding: "20px" }}>
        <div className="email">
          <label htmlFor="email">Email</label>
          <input
            style={{ padding: "10" }}
            type="text"
            placeholder="Username"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="password" style={{ padding: "10" }}>
          <label style={{ padding: "10" }} htmlFor="password">
            Password
          </label>
          <input
            style={{ padding: "10" }}
            type="password"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="login">
          <button onClick={loginUser}>Login</button>
          <Link to="/signup">Signup</Link>
        </div>
        <div className="google">
          <button onClick={googleLogin}>Google</button>
        </div>
      </div>
    </>
  );
}
