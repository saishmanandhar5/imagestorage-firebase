import React, { useState } from "react";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";

export default function Signup() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const auth = getAuth();
  const createUser = async () => {
    await createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log(user);
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        alert(errorCode);
      });
  };

  return (
    <>
      <div className="container" style={{ padding: "20px" }}>
        <div className="email">
          <label htmlFor="email">Email</label>
          <input
            style={{ padding: "10" }}
            type="text"
            placeholder="Username"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="password" style={{ padding: "10" }}>
          <label style={{ padding: "10" }} htmlFor="password">
            Password
          </label>
          <input
            style={{ padding: "10" }}
            type="password"
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="signup">
          <button onClick={createUser}>Signup</button>
        </div>
      </div>
    </>
  );
}
