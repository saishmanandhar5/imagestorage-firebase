import React from "react";
import { useState, useEffect } from "react";
import { storage } from "./firebase";
import { ref, uploadBytes, listAll, getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";
import { getAuth, signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import userEvent from "@testing-library/user-event";

export default function Image() {
  const navigate = useNavigate();
  const [imageUpload, setImageUpload] = useState(null);
  const [imageList, setImageList] = useState([]);

  const imageListRef = ref(storage, "images/");
  const uploadImage = () => {
    if (imageUpload == null) return;

    var imageRef = ref(storage, `images/${imageUpload.name + v4()}`);
    uploadBytes(imageRef, imageUpload).then(() => {
      alert("image uploaded");
    });
  };
  useEffect(() => {
    console.log("in");
    listAll(imageListRef).then((res) => {
      res.items.forEach((item) => {
        getDownloadURL(item).then((url) => {
          setImageList((prev) => [...prev, url]);
          console.log(url);
        });
      });
    });

    const value = localStorage.getItem("user");
    if (!value) {
      navigate("/");
    }
  }, []);
  const auth = getAuth();
  const signOutUser = () => {
    signOut(auth)
      .then(() => {
        console.log("Sign - out successful.");
        localStorage.clear();
      })
      .catch((error) => {
        console.log(error);
      });
    navigate("/");
  };
  return (
    <>
      <div className="add">
        <input
          type="file"
          onChange={(e) => {
            setImageUpload(e.target.files[0]);
          }}
        />

        <button onClick={uploadImage}>Upload Image</button>

        <button onClick={signOutUser}>SignOut</button>
        <span></span>

        <div>
          {imageList.map((url, idx) => {
            return (
              <img
                src={url}
                key={idx}
                style={{ height: "300px", width: "250px", padding: "4rem" }}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}
